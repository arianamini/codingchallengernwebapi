const Joi = require("joi")

function validateBody(body) {
	const schema = Joi.object({
		name: Joi.string().trim().min(2).max(50).required(),
	})

	return schema.validate(body)
}

exports.validateBody = validateBody
