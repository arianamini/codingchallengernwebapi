const express = require("express")
const router = express.Router()

const { validateBody } = require("../models/api")

router.post("/reactnative", async (req, res) => {
	try {
		const { error } = validateBody(req.body)
		if (error)
			return res.status(400).send({ message: error.details[0].message })

		var randomBoolean = Math.random() < 0.5
		if (randomBoolean)
			return res.status(401).send({ message: "Sample error message" })

		const returnValue = {
			name: req.body.name,
			dailyProgress: Math.floor(Math.random() * 101),
			monthlyProgress: Math.floor(Math.random() * 101),
			yearlyProgress: Math.floor(Math.random() * 101),
		}

		return res.send(returnValue)
	} catch (err) {
		console.log("AAABB err: " + err)
		return res.status(500).send({ message: "CCRN.1.21: " + err })
	}
})

module.exports = router
