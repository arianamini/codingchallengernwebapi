const express = require("express")
const api = require("../routes/api")

const error = require("../middleware/error")

module.exports = function (app) {
	app.use(express.json())
	app.use("/api/v1/challenge", api)

	app.use(error)
}
